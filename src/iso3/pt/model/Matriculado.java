package iso3.pt.model;

public class Matriculado {
	
	private Integer dniAlumno;
	private Integer idAsignatura;
	
	public Matriculado(){}
	
	
	
	public Matriculado(Integer dniAlumno, Integer idAsignatura) {
		super();
		this.dniAlumno = dniAlumno;
		this.idAsignatura = idAsignatura;
	}



	public Integer getDniAlumno() {
		return dniAlumno;
	}
	public void setDniAlumno(Integer dniAlumno) {
		this.dniAlumno = dniAlumno;
	}
	public Integer getIdAsignatura() {
		return idAsignatura;
	}
	public void setIdAsignatura(Integer idAsignatura) {
		this.idAsignatura = idAsignatura;
	}
	

}
