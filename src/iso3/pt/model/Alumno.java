package iso3.pt.model;

import java.util.HashSet;
import java.util.Set;

public class Alumno {
	private int dni;
	private String password;
	private String nombre;
	private String telefono;
	
	private Set<Asignatura> listaAsignaturas;
	private Set<Evaluacion> listaEvaluaciones;
	
	public Alumno() {
	}

	public Alumno(int dni, String password, String nombre, String telefono) {
		super();
		this.dni = dni;
		this.password = password;
		this.nombre = nombre;
		this.telefono = telefono;
		this.listaAsignaturas =  new HashSet<Asignatura>();
		this.listaEvaluaciones =  new HashSet<Evaluacion>();
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Set<Asignatura> getListaAsignaturas() {
		return listaAsignaturas;
	}

	public void setListaAsignaturas(Set<Asignatura> listaAsignaturas) {
		this.listaAsignaturas = listaAsignaturas;
	}
	
	public void addAsignatura(Asignatura a) {
		this.listaAsignaturas.add(a);
	}
	
	public void removeAsignatura(Asignatura a) {
		this.listaAsignaturas.remove(a);
	}

	public void setListaEvaluaciones(Set<Evaluacion> listaEvaluaciones) {
		this.listaEvaluaciones = listaEvaluaciones;
	}

	public Set<Evaluacion> getListaEvaluaciones() {
		return listaEvaluaciones;
	}
}
