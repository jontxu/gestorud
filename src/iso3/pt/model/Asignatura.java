package iso3.pt.model;

import java.util.HashSet;
import java.util.Set;

public class Asignatura {
	
	private int id;
	private int codigo;
	private String nombre;
	private float creditos;
	
	private Set<Alumno> listaAlumnos;
	private Set<Unidad> listaUnidades;
	private Profesor profesor;
	
	public Asignatura() {
	}
	
	public Asignatura(int codigo, String nombre, float creditos) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.creditos = creditos;
		this.listaAlumnos =  new HashSet<Alumno>();
		this.listaUnidades =  new HashSet<Unidad>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getCreditos() {
		return creditos;
	}

	public void setCreditos(float creditos) {
		this.creditos = creditos;
	}

	public Set<Alumno> getListaAlumnos() {
		return listaAlumnos;
	}

	public void setListaAlumnos(Set<Alumno> listaAlumnos) {
		this.listaAlumnos = listaAlumnos;
	}
	
	public void addAlumno(Alumno a) {
		this.listaAlumnos.add(a);
	}
	
	public void removeAlumno(Alumno a) {
		this.listaAlumnos.remove(a);
	}

	public Set<Unidad> getListaUnidades() {
		return listaUnidades;
	}

	public void setListaUnidades(Set<Unidad> listaUnidades) {
		this.listaUnidades = listaUnidades;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public Profesor getProfesor() {
		return profesor;
	}
}
