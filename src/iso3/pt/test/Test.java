package iso3.pt.test;

import org.hibernate.*;

import org.hibernate.cfg.Configuration;

import iso3.pt.model.*;

public class Test { 
	
		SessionFactory sessionFactory;
		
		public Test(){
			sessionFactory = new Configuration().configure().buildSessionFactory();
		}
	
		public void inserciones() {
	    	
	        Session session = sessionFactory.openSession();
	        Transaction tx = session.beginTransaction();
	             
	        Profesor p1 = new Profesor(100, "pass0", "nomb0", "tel0", "a@a.com", "d0");
	        Profesor p2 = new Profesor(101, "pass1", "nomb1", "tel1", "b@b.com", "d1");
	        
	        Alumno a1 = new Alumno(000, "pass0", "nombre0", "tel0");
	        Alumno a2 = new Alumno(001, "pass1", "nombre1", "tel1");
	        
	        Asignatura as1 = new Asignatura(200, "nombre0", 0.0f);
	        Asignatura as2 = new Asignatura(201, "nombre1", 1.1f);
	        
	        Unidad u1 = new Unidad("acr0", "tit0", "cont0");
	        Unidad u2 = new Unidad("acr1", "tit1", "cont1");
	        
	        Evaluacion e1 = new Evaluacion("conc0", 0.5f);
	        Evaluacion e2 = new Evaluacion("conc1", 0.6f);

	        as1.setProfesor(p1);
	        as2.setProfesor(p2);
	        as1.getListaAlumnos().add(a1);
	        as2.getListaAlumnos().add(a2);
	        as1.getListaUnidades().add(u1);
	        as2.getListaUnidades().add(u2);
	        
	        e1.setAlumno(a1);
	        e1.setAsignatura(as1);
	        e2.setAlumno(a2);
	        e2.setAsignatura(as2);
	        
	        a1.getListaEvaluaciones().add(e1);
	        a2.getListaEvaluaciones().add(e2);
	        
	        session.save(p1);
	        session.save(p2);
	        session.save(as1);
	        session.save(as2);
	        session.save(u1);
	        session.save(u2);
	        session.save(a1);
	        session.save(a2);
	        session.save(e1);
	        session.save(e2);
	        
	        tx.commit();
	        session.close();
	        System.out.println("Done inserciones!");
		}
		
		public void close(){
	        sessionFactory.close();
		}

	    public static void main(String[] args) {
	    	Test t1 = new Test();
	    	t1.inserciones();
	    	t1.close();
	    }
	}
