package iso3.pt.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import iso3.pt.model.Alumno;
import iso3.pt.model.Asignatura;
import iso3.pt.model.Evaluacion;
import iso3.pt.service.PtDaoService;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

public class SubjectMarkingAction extends ActionSupport implements Preparable
{
	private Alumno alumno;
	private int dniAlumno;
	private Asignatura asignatura;
	private int idAsignatura;
	private List<Evaluacion> listaEvaluaciones = new ArrayList<Evaluacion>();
	
	private String concepto;
	private float nota;
	PtDaoService daos = new PtDaoService();

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		alumno = daos.getAlumno(dniAlumno);
		asignatura = daos.getAsignatura(idAsignatura);
		
		Set<Evaluacion> evals = daos.getEvaluaciones(idAsignatura, dniAlumno);
		for (Evaluacion e: evals) {
			listaEvaluaciones.add(e);
		}
		
	}
	
	
	public String execute() throws Exception {
		alumno = daos.getAlumno(dniAlumno);
		asignatura = daos.getAsignatura(idAsignatura);
		listaEvaluaciones = new ArrayList<Evaluacion>();
		
		Set<Evaluacion> evals = daos.getEvaluaciones(idAsignatura, dniAlumno);
		for (Evaluacion e: evals) {
			listaEvaluaciones.add(e);
		}
		
		return "markingform";
	}

	public String introducirNota()
	{
		daos.addEvaluacion(concepto, nota, idAsignatura, dniAlumno);
		Set<Evaluacion> evals = daos.getEvaluaciones(idAsignatura, dniAlumno);
		listaEvaluaciones = new ArrayList<Evaluacion>();
		for (Evaluacion e: evals) {
			listaEvaluaciones.add(e);
		}
		return "introNota";
	}
	
	public String doCancel()
	{
		return "cancel";
	}
	
	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public int getDniAlumno() {
		return dniAlumno;
	}

	public void setDniAlumno(int dniAlumno) {
		this.dniAlumno = dniAlumno;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public int getIdAsignatura() {
		return idAsignatura;
	}

	public void setIdAsignatura(int idAsignatura) {
		this.idAsignatura = idAsignatura;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public float getNota() {
		return nota;
	}

	public void setNota(float nota) {
		this.nota = nota;
	}

	public List<Evaluacion> getListaEvaluaciones() {
		return listaEvaluaciones;
	}

	public void setListaEvaluaciones(List<Evaluacion> listaEvaluaciones) {
		this.listaEvaluaciones = listaEvaluaciones;
	}
}
