package iso3.pt.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import iso3.pt.model.Alumno;
import iso3.pt.model.Asignatura;
import iso3.pt.model.Evaluacion;
import iso3.pt.model.Unidad;
import iso3.pt.service.PtDaoService;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

public class SubjectAction extends ActionSupport implements Preparable
{

	private Alumno alumno;
	private int dniAlumno;
	private Asignatura asignatura;
	private int idAsignatura;
	private List<Unidad> listaUnidades;
	private List<Evaluacion> listaNotas;
	private List<Evaluacion> listaEvaluaciones = new ArrayList<Evaluacion>();
	PtDaoService daos = new PtDaoService();
	
	
	@Override
	public void prepare() throws Exception 
	{
		alumno = daos.getAlumno(dniAlumno);
		asignatura = daos.getAsignatura(idAsignatura);
	}
	
	public String verNotasAlumno() {
		alumno = daos.getAlumno(dniAlumno);
		asignatura = daos.getAsignatura(idAsignatura);
		listaEvaluaciones = new ArrayList<Evaluacion>();
		
		Set<Evaluacion> evals = daos.getEvaluaciones(idAsignatura, dniAlumno);
		for (Evaluacion e: evals) {
			listaEvaluaciones.add(e);
		}
		
		return "vernotasalumno";
	}

	public String generarUnidadesDeAsignatura() 
	{
		listaUnidades = new ArrayList<Unidad>(); 
		for (Unidad u : daos.getUnidades(idAsignatura)) 
		{
			listaUnidades.add(u);
		}
		return "generarUnidadesDeAsignatura";
	}
	
	public String generarTodasLasNotasAlumno()
	{
		listaNotas = new ArrayList<Evaluacion>();
		
		for(Evaluacion e: alumno.getListaEvaluaciones()) 
		{
			listaNotas.add(e);
		}
		return "generarNotasDelAlumno";
	}
	
	public String generarNotasDeAsignatura()
	{
		asignatura = daos.getAsignatura(idAsignatura);
		alumno = daos.getAlumno(dniAlumno);
		listaNotas = new ArrayList<Evaluacion>();
		
		for (Evaluacion eval: daos.getEvaluaciones(idAsignatura, dniAlumno)) {
			listaNotas.add(eval);
		}
		return "generarNotasAsignatura";
		
	}
	
	// getters & setters

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public int getDniAlumno() {
		return dniAlumno;
	}

	public void setDniAlumno(int dniAlumno) {
		this.dniAlumno = dniAlumno;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public int getIdAsignatura() {
		return idAsignatura;
	}

	public void setIdAsignatura(int idAsignatura) {
		this.idAsignatura = idAsignatura;
	}

	public List<Unidad> getListaUnidades() {
		return listaUnidades;
	}

	public void setListaUnidades(List<Unidad> listaUnidades) {
		this.listaUnidades = listaUnidades;
	}

	public List<Evaluacion> getListaNotas() {
		return listaNotas;
	}

	public void setListaNotas(List<Evaluacion> listaNotas) {
		this.listaNotas = listaNotas;
	}
	
	public List<Evaluacion> getListaEvaluaciones() {
		return listaEvaluaciones;
	}

	public void setListaEvaluaciones(List<Evaluacion> listaEvaluaciones) {
		this.listaEvaluaciones = listaEvaluaciones;
	}

	
}
