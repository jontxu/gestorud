package iso3.pt.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import iso3.pt.model.Alumno;
import iso3.pt.model.Asignatura;
import iso3.pt.model.Profesor;
import iso3.pt.service.PtDaoService;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

public class LecturerAction extends ActionSupport implements Preparable
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int idAsignatura;
	private String nombreAsignatura;
	private Profesor profesor;
	private int dniEstudiante;
	private List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();
	private List<Alumno> listaEstudiantes = new ArrayList<Alumno>();
	private PtDaoService daos = new PtDaoService();
	
	


	@Override
	public void prepare() throws Exception 
	{
		// TODO Auto-generated method stub
		Map session = ActionContext.getContext().getSession();
		profesor = (Profesor) session.get("profesor");
		
		
		Set<Asignatura> asignaturas = daos.getAsignaturasProfesor(profesor.getId());
		for (Asignatura a: asignaturas) {
			listaAsignaturas.add(a);
		}
	}
	
/*	public String execute() throws Exception 
	{		
		return SUCCESS;
	}
	*/
	
	public String generarListaEstudiantes() throws Exception 
	{
		listaEstudiantes = new ArrayList<Alumno>();
	
		nombreAsignatura = daos.getAsignatura(getIdAsignatura()).getNombre();
		
		try {
			if(profesor != null) {	
				for (Alumno alumn : daos.getAlumnos(idAsignatura))
					listaEstudiantes.add(alumn);
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		return "listaEstudiantes";
	}
	
	public String doLogout()
	{
		Map session = ActionContext.getContext().getSession();
	  
	  	if (session.get("profesor") != null)
	  		session.remove("profesor"); 
		
		return "logoutp";
	}

	public int getIdAsignatura() {
		return idAsignatura;
	}

	public void setIdAsignatura(int idAsignatura) {
		this.idAsignatura = idAsignatura;
	}

	public String getNombreAsignatura() {
		return nombreAsignatura;
	}

	public void setNombreAsignatura(String nombreAsignatura) {
		this.nombreAsignatura = nombreAsignatura;
	}

	public Profesor getProfesor() {
		return profesor;
	}
	
	public int getDNI() {
		return profesor.getDni();
	}
	
	public String getNombre() {
		return profesor.getNombre();
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public int getDniEstudiante() {
		return dniEstudiante;
	}

	public void setDniEstudiante(int dniEstudiante) {
		this.dniEstudiante = dniEstudiante;
	}

	public List<Asignatura> getListaAsignaturas() {
		return listaAsignaturas;
	}

	public void setListaAsignaturas(List<Asignatura> listaAsignaturas) {
		this.listaAsignaturas = listaAsignaturas;
	}

	public List<Alumno> getListaEstudiantes() {
		return listaEstudiantes;
	}

	public void setListaEstudiantes(List<Alumno> listaEstudiantes) {
		this.listaEstudiantes = listaEstudiantes;
	}

	public PtDaoService getDaos() {
		return daos;
	}

	public void setDaos(PtDaoService daos) {
		this.daos = daos;
	}
	
}
