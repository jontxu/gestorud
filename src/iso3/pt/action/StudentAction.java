package iso3.pt.action;

import iso3.pt.model.Alumno;
import iso3.pt.model.Asignatura;
import iso3.pt.model.Evaluacion;
import iso3.pt.model.Profesor;
import iso3.pt.service.PtDaoService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

public class StudentAction extends ActionSupport implements Preparable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Alumno alumno;
	private int dniAlumno;
	private Asignatura asignatura;
	private int idAsignatura;
	private int idSubject;
	private List<Asignatura> listaAsignaturas = new ArrayList<Asignatura>();
	private PtDaoService daos = new PtDaoService();
	private String nombreAsignatura;
	private List<Evaluacion> listaEvaluaciones = new ArrayList<Evaluacion>();

	@Override
	public void prepare() throws Exception {
		listaAsignaturas = new ArrayList<Asignatura>();
		Map session = ActionContext.getContext().getSession();
		alumno = (Alumno) session.get("alumno");
		Set<Asignatura> asignaturas = daos.getAsignaturas(alumno.getDni());
		for (Asignatura a: asignaturas) {
			listaAsignaturas.add(a);
			System.out.println(a.getNombre());
		}
		Set<Evaluacion> evals = daos.getEvaluaciones(idAsignatura, alumno.getDni());
		for (Evaluacion e: evals) {
			listaEvaluaciones.add(e);
		}
	}
	
	/*	
	public String execute() throws Exception
	{
	}
	*/
	
	private void getAsignaturas() {
		listaAsignaturas = new ArrayList<Asignatura>();
		Set<Asignatura> asignaturas = daos.getAsignaturas(alumno.getDni());
		for (Asignatura a: asignaturas) {
			listaAsignaturas.add(a);
		}
	}
	

	public String getListaTodasAsignaturas() {
		Set<Asignatura> asignaturas = daos.getAsignaturas();
		listaAsignaturas = new ArrayList<Asignatura>();
		for (Asignatura a: asignaturas) {
			listaAsignaturas.add(a);
		}
		nombreAsignatura = listaAsignaturas.get(0).getNombre();
		return "listaTodasAsignaturas";
	}
	
	public String matricular() {
		daos.matricular(dniAlumno, idSubject);
		getAsignaturas();
		return "matricular";
	}
	
	public String desmatricular() {
		System.out.println("dni:");
		daos.desmatricular(dniAlumno, idAsignatura);
		getAsignaturas();
		return "desmatricular";
	}
	
	public String logout() {
		Map session = ActionContext.getContext().getSession();
	  
	  	if (session.get("alumno") != null)
	  		session.remove("alumno"); 
		
		return "logout";
	}
	
	public List<Asignatura> getListaAsignaturas() {
		return listaAsignaturas;
	}

	public void setListaAsignaturas(List<Asignatura> listaAsignaturas) {
		this.listaAsignaturas = listaAsignaturas;
	}
	
	public Alumno getAlumno() {
		return alumno;
	}

	public String getNombreAsignatura() {
		return nombreAsignatura;
	}

	public void setNombreAsignatura(String nombreAsignatura) {
		this.nombreAsignatura = nombreAsignatura;
	}


	public Asignatura getAsignatura() {
		return asignatura;
	}


	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public List<Evaluacion> getListaEvaluaciones() {
		return listaEvaluaciones;
	}

	public void setListaEvaluaciones(List<Evaluacion> listaEvaluaciones) {
		this.listaEvaluaciones = listaEvaluaciones;
	}

	public int getDniAlumno() {
		return dniAlumno;
	}

	public void setDniAlumno(int dniAlumno) {
		this.dniAlumno = dniAlumno;
	}

	public int getIdSubject() {
		return idSubject;
	}

	public void setIdSubject(int id) {
		this.idSubject = id;
	}
	
	public int getIdAsignatura() {
		return idAsignatura;
	}

	public void setIdAsignatura(int id) {
		this.idAsignatura = id;
	}
}
