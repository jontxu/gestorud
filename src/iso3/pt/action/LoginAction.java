package iso3.pt.action;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;




import iso3.pt.dao.IncorrectPasswordException;
import iso3.pt.dao.PtDAO;
import iso3.pt.dao.UserNotFoundException;
import iso3.pt.model.Alumno;
import iso3.pt.model.Profesor;
import iso3.pt.service.PtDaoService;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


/**
 * <p> Validate a user login. </p>
 */
public class LoginAction  extends ActionSupport implements Preparable
{
	   
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int username;
    private String password;
    private String listaTipo; 

    
    public String execute() throws Exception {
    	
    
    	PtDaoService daos = new PtDaoService();
    	Map datosSession = ActionContext.getContext().getSession();
   
    	
    	if(this.getListaTipo().equals(getText("label.login.lecturer")))
    	{
    		
			try {
				datosSession.put("profesor", daos.loginProfesor(username, password));
				return "loginProfesor";
			} catch (UserNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return ERROR;
			} catch (IncorrectPasswordException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return ERROR;
			} 
    	}
    	else
    	{
			try {
				datosSession.put("alumno", daos.loginAlumno(username, password));
				return "loginAlumno";
			} catch (UserNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return ERROR;
			} catch (IncorrectPasswordException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return ERROR;
			}
	
	    		
    	}
	}
    
    

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
    
	public String doLogout()
	{
		return null;
	}
    


    // ---- Username property ----

    /**
     * <p>Field to store User username.</p>
     * <p/>
     */
 


    /**
     * <p>Provide User username.</p>
     *
     * @return Returns the User username.
     */
    public int getUsername() {
        return username;
    }

    /**
     * <p>Store new User username</p>
     *
     * @param value The username to set.
     */
    public void setUsername(int value) {
        username = value;
    }

    // ---- Username property ----

    /**
     * <p>Field to store User password.</p>
     * <p/>
     */



    /**
     * <p>Provide User password.</p>
     *
     * @return Returns the User password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * <p>Store new User password</p>
     *
     * @param value The password to set.
     */
    public void setPassword(String value) {
        password = value;
    }




	public String getListaTipo() {
		return listaTipo;
	}




	public void setListaTipo(String tipo) {
		this.listaTipo = tipo;
	}









}
