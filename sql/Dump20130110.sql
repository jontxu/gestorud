CREATE DATABASE  IF NOT EXISTS `iso3pt` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `iso3pt`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: iso3pt
-- ------------------------------------------------------
-- Server version	5.5.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alumno`
--

DROP TABLE IF EXISTS `alumno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alumno` (
  `DNI` int(11) NOT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `TELEFONO` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`DNI`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alumno`
--

LOCK TABLES `alumno` WRITE;
/*!40000 ALTER TABLE `alumno` DISABLE KEYS */;
INSERT INTO `alumno` VALUES (0,'pass0','nombre0','tel0'),(1,'pass1','nombre1','tel1');
/*!40000 ALTER TABLE `alumno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asignatura`
--

DROP TABLE IF EXISTS `asignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asignatura` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODIGO` int(11) NOT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `CREDITOS` float DEFAULT NULL,
  `PROFESOR_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `CODIGO` (`CODIGO`),
  KEY `FKECBB61734A862E49` (`PROFESOR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asignatura`
--

LOCK TABLES `asignatura` WRITE;
/*!40000 ALTER TABLE `asignatura` DISABLE KEYS */;
INSERT INTO `asignatura` VALUES (2,200,'nombre0',0,3),(3,201,'nombre1',1.1,4);
/*!40000 ALTER TABLE `asignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evaluacion`
--

DROP TABLE IF EXISTS `evaluacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evaluacion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONCEPTO` varchar(255) NOT NULL,
  `NOTA` float NOT NULL,
  `ASIGNATURA_ID` int(11) DEFAULT NULL,
  `ALUMNO_DNI` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK500E38ED55D41DE9` (`ASIGNATURA_ID`),
  KEY `FK500E38ED27D1A0F7` (`ALUMNO_DNI`),
  CONSTRAINT `FK500E38ED27D1A0F7` FOREIGN KEY (`ALUMNO_DNI`) REFERENCES `alumno` (`DNI`),
  CONSTRAINT `FK500E38ED55D41DE9` FOREIGN KEY (`ASIGNATURA_ID`) REFERENCES `asignatura` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evaluacion`
--

LOCK TABLES `evaluacion` WRITE;
/*!40000 ALTER TABLE `evaluacion` DISABLE KEYS */;
INSERT INTO `evaluacion` VALUES (1,'conc0',0.5,2,0),(2,'conc1',0.6,3,1),(26,'conc1',10,2,0),(27,'Lol',0,2,0),(28,'Examen final',0,2,0),(29,'Cero',0,2,0),(30,'Prueba',0,2,0),(31,'Test',0,3,1);
/*!40000 ALTER TABLE `evaluacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matriculado`
--

DROP TABLE IF EXISTS `matriculado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matriculado` (
  `ALUMNO_DNI` int(11) NOT NULL,
  `ASIGNATURA_ID` int(11) NOT NULL,
  PRIMARY KEY (`ALUMNO_DNI`,`ASIGNATURA_ID`),
  KEY `FK48D1366955D41DE9` (`ASIGNATURA_ID`),
  KEY `FK48D1366927D1A0F7` (`ALUMNO_DNI`),
  CONSTRAINT `FK48D1366927D1A0F7` FOREIGN KEY (`ALUMNO_DNI`) REFERENCES `alumno` (`DNI`),
  CONSTRAINT `FK48D1366955D41DE9` FOREIGN KEY (`ASIGNATURA_ID`) REFERENCES `asignatura` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matriculado`
--

LOCK TABLES `matriculado` WRITE;
/*!40000 ALTER TABLE `matriculado` DISABLE KEYS */;
INSERT INTO `matriculado` VALUES (0,2),(1,3);
/*!40000 ALTER TABLE `matriculado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesor`
--

DROP TABLE IF EXISTS `profesor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesor` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DNI` int(11) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `NOMBRE` varchar(255) NOT NULL,
  `TELEFONO` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `DESPACHO` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NOMBRE` (`NOMBRE`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesor`
--

LOCK TABLES `profesor` WRITE;
/*!40000 ALTER TABLE `profesor` DISABLE KEYS */;
INSERT INTO `profesor` VALUES (3,100,'pass0','nomb0','tel0','a@a.com','d0'),(4,101,'pass1','nomb1','tel1','b@b.com','d1');
/*!40000 ALTER TABLE `profesor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidad`
--

DROP TABLE IF EXISTS `unidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidad` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACRONIMO` varchar(255) DEFAULT NULL,
  `TITULO` varchar(255) NOT NULL,
  `CONTENIDO` varchar(255) DEFAULT NULL,
  `ASIGNATURA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ACRONIMO` (`ACRONIMO`),
  KEY `FK95794C9755D41DE9` (`ASIGNATURA_ID`),
  CONSTRAINT `FK95794C9755D41DE9` FOREIGN KEY (`ASIGNATURA_ID`) REFERENCES `asignatura` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidad`
--

LOCK TABLES `unidad` WRITE;
/*!40000 ALTER TABLE `unidad` DISABLE KEYS */;
INSERT INTO `unidad` VALUES (1,'acr0','tit0','cont0',2),(2,'acr1','tit1','cont1',3);
/*!40000 ALTER TABLE `unidad` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-01-10 19:41:45
