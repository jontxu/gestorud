<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.subjectunitlist.title"/> <s:property value="%{asignatura.nombre}"/></h1>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.subjectunitlist.id"/></th>
		        <th><s:text name="label.subjectunitlist.acr"/></th>
		        <th><s:text name="label.subjectunitlist.tit"/></th>
		        <th><s:text name="label.subjectunitlist.content"/></th>
		    </tr>
		    <s:iterator value="listaUnidades" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{id}"/></td>
		            <td class="nowrap"><s:property value="%{acronimo}"/></td>
		            <td class="nowrap"><s:property value="%{titulo}"/></td>
		            <td class="nowrap"><s:property value="%{contenido}"/></td>
		         </tr>
		    </s:iterator>
		     
		</table>
		<s:actionerror />
		<s:actionmessage />
	</body>
</html>