<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<%! LecturerAction action = (LecturerAction)ActionContext.getContext().getActionInvocation().getAction();  %>


<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.subjectstudents.teacher"/> "<s:property value="%{profesor.nombre}"/>" <s:text name="label.subjectstudents.subject"/> "<s:property value="%{nombreAsignatura}"/>"</h1>
		<br/>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.subjectstudents.nid"/></th>
		        <th><s:text name="label.subjectstudents.name"/></th>
		        <th><s:text name="label.subjectstudents.tlf"/></th>
		        <th><s:text name="label.subjectstudents.id"/></th>
		        <th>&nbsp;&nbsp;</th>
		        <th>&nbsp;&nbsp;</th>
		    </tr>
		    <s:iterator value="listaEstudiantes" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{dni}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{telefono}"/></td>
		            <td class="nowrap">
		            	<s:url id="addgrade" action="subjectMarking" escapeAmp="false">
			            	<s:param name="idAsignatura"><s:property value="%{idAsignatura}"/></s:param>
			                <s:param name="dniAlumno" value="%{dni}" />
					    </s:url>
			            <s:a href="%{addgrade}"><s:text name="label.subjectstudents.add"/></s:a>
			        </td>
		        </tr>
		        </s:iterator>
		</table>
		<s:actionerror />
		<s:actionmessage />
		<s:form action="subjectMarking!cancel" method="POST">
			<s:submit value="%{getText('label.logout.button')}" align="center"/>
		</s:form>
	</body>