<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.studentsubjectrm.title"/> "<s:property value="%{alumno.nombre}"/>"</h1>
		<br/>
 		
		<s:form action="listaAsignaturasAlumno!matricular" method="POST">
			<s:hidden name="dniAlumno" value="%{dniAlumno}"/>
			<s:select list="listaAsignaturas" label="%{getText('label.studentsubjectrm.name')}" value="idSubject" name="idSubject" listKey="id" listValue="nombre"/>
		<s:submit value="%{getText('label.studentsubjectrm.give')}" align="right" />
		</s:form>
		<s:form action="listaAsignaturasAlumno!input" method="POST">
		<s:submit name="idAsignatura" value="%{getText('label.studentsubjectrm.cancel')}" align="right" />
		</s:form>
		<s:actionerror />
		<s:actionmessage />
	</body>
</html>