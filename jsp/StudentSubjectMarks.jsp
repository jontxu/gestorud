<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>



<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.studentsubjectsm.titleone"/> "<s:property value="%{alumno.nombre}"/>" <s:text name="label.studentsubjectsm.titletwo"/> "<s:property value="%{asignatura.nombre}"/>"</h1>
		<br/>
		<br/>
				<table class="borderAll">
		    <tr>
		        <th><s:text name="label.studentsubjectsm.nid"/></th>
		        <th><s:text name="label.studentsubjectsm.name"/></th>
		        <th><s:text name="label.studentsubjectsm.tlf"/></th>
		    </tr>
		    <s:iterator value="alumno" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{dni}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{telefono}"/></td>
		        </tr>
		    </s:iterator>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		       	<th><s:text name="label.studentsubjectsm.code"/></th>
		        <th><s:text name="label.studentsubjectsm.name"/></th>
		        <th><s:text name="label.studentsubjectsm.credits"/></th>
		        <th><s:text name="label.studentsubjectsm.lecturer"/></th>
		        <th><s:text name="label.studentsubjectsm.units"/></th>
		    </tr>
		    <s:iterator value="asignatura" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{codigo}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{creditos}"/></td>
		            <td class="nowrap"><s:property value="%{profesor.nombre}"/></td>
		            <td class="nowrap"><s:property value="%{listaAlumnos.size}"/></td>
		        </tr>
		    </s:iterator>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		       	<th><s:text name="label.studentsubjectsm.id"/></th>
		        <th><s:text name="label.studentsubjectsm.concept"/></th>
		        <th><s:text name="label.studentsubjectsm.grade"/></th>
		    </tr>
		<s:iterator value="listaEvaluaciones" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{id}"/></td>
		            <td class="nowrap"><s:property value="%{concepto}"/></td>
		            <td class="nowrap"><s:property value="%{nota}"/></td>
		        </tr>
		</s:iterator>
		</table>
		<br/>
		<s:form action="subjectMarking!doCancel" method="POST">
			<s:submit value="%{getText('label.studentsubjectsm.cancel')}" align="center"/>
		</s:form>
		<s:actionerror />
		<s:actionmessage />
	</body>
</html>