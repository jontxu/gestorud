<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>



<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.studentsubjectsmk.title"/>:</h1>
		<br/>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.studentsubjectsmk.nid"/></th>
		        <th><s:text name="label.studentsubjectsmk.name"/></th>
		        <th><s:text name="label.studentsubjectsmk.tlf"/></th>
		    </tr>
		    <s:iterator value="alumno" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{dni}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{telefono}"/></td>
		        </tr>
		    </s:iterator>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		       	<th><s:text name="label.studentsubjectsmk.code"/></th>
		        <th><s:text name="label.studentsubjectsmk.name"/></th>
		        <th><s:text name="label.studentsubjectsmk.credits"/></th>
		        <th><s:text name="label.studentsubjectsmk.lecturer"/></th>
		        <th><s:text name="label.studentsubjectsmk.units"/></th>
		    </tr>
		    <s:iterator value="asignatura" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{codigo}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{creditos}"/></td>
		            <td class="nowrap"><s:property value="%{profesor.nombre}"/></td>
		            <td class="nowrap"><s:property value="%{listaAlumnos.size}"/></td>
		        </tr>
		    </s:iterator>
		</table>
		<s:actionerror />
		<s:actionmessage />
		<s:form action="subjectMarking!introducirNota" method="POST">
			<tr>
				<td colspan="2">
					<s:actionerror />
					<!--<s:fielderror />-->
				</td>
			</tr>
			<s:textfield name="concepto" label="%{getText('label.studentsubjectsmk.concept')}"/>
			<s:textfield name="nota" label="%{getText('label.studentsubjectsmk.grade')}"/>
			<s:hidden name="idAsignatura" value="%{idAsignatura}"/>
			<s:hidden name="dniAlumno" value="%{dniAlumno}"/>
			<s:submit value="%{getText('label.studentsubjectsmk.give')}" align="center"/>
		</s:form>
		<s:form action="subjectMarking!doCancel" method="POST">
			<s:submit value="%{getText('label.studentsubjectsmk.cancel')}" align="center"/>
		</s:form>
	</body>