<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
	<head>
		<title><s:text name="application.title"/></title>
		<link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
	</head>
	<body>
	<div class="titleDiv"><s:text name="application.title"/></div>
		<h3><s:text name="label.login"/></h3>
		<s:form action="doLogin" method="POST">
			<tr>
				<td colspan="2">Login</td>
			</tr>
			<tr>
				<td colspan="2">
					<s:actionerror />
					<!--<s:fielderror />-->
				</td>
			</tr>
			<s:textfield name="username" label="Usuario"/>
			<s:password name="password" label="Contraseña"/>
			<s:select name="listaTipo" list="{getText('label.login.student'), getText('label.login.lecturer')}" label="%{getText('label.login.type')}" />
			<s:submit name="Acceder" value="Login" align="center"/>
		</s:form>
	</body>
</html>