<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.studentsubjects.title"/> <s:property value="%{alumno.nombre}"/> (<s:property value="%{alumno.dni}"/>)</h1> 
		<br/>
		<table>
			<tr>
				<td>
					<s:url id="enroll" action="listaAsignaturasAlumno!getListaTodasAsignaturas">
						<s:param name="dniAlumno" value="%{alumno.dni}" />
					</s:url>
					<s:a href="%{enroll}"><s:text name="label.studentsubjects.register"/></s:a>
				</td>
				<td>
					<s:url id="grades" action="subject!generarTodasLasNotasAlumno">
			 			<s:param name="dniAlumno" value="%{alumno.dni}" />
					</s:url>
					<s:a href="%{grades}"><s:text name="label.studentsubjects.grades"/></s:a>
				</td>
				<td>
					<s:url id="logout" action="listaAsignaturasAlumno!logout"/>
					<s:a href="%{logout}"><s:text name="label.studentsubjects.logout"/></s:a>
				</td>
			</tr>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.studentsubjects.code"/></th>
		        <th><s:text name="label.studentsubjects.name"/></th>
		        <th><s:text name="label.studentsubjects.credits"/></th>
		        <th><s:text name="label.studentsubjects.lecturer"/></th>
		        <th><s:text name="label.studentsubjects.units"/></th>
		        <th><s:text name="label.studentsubjects.students"/></th>
		        <th>&nbsp;&nbsp;</th>
		        <th>&nbsp;&nbsp;</th>
		    </tr>
		    <s:iterator value="listaAsignaturas" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{codigo}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{creditos}"/></td>
		            <td class="nowrap"><s:property value="%{profesor.nombre}"/></td>
		            <td class="nowrap">
		            <s:url id="unidades" action="subject!generarUnidadesDeAsignatura">
			                    <s:param name="idAsignatura" value="%{id}" />
					    </s:url>
			            <s:a href="%{unidades}"><s:property value="%{listaUnidades.size}"/></s:a>
		            </td>
		            <td class="nowrap"><s:property value="%{listaAlumnos.size}"/></td>
		            <td class="nowrap">
		            	<s:url id="unenroll" action="listaAsignaturasAlumno!desmatricular">
			                    <s:param name="dniAlumno" value="%{alumno.dni}" />
			                    <s:param name="idAsignatura" value="%{id}" />      
			            </s:url>
			            <s:a href="%{unenroll}"><s:text name="label.studentsubjects.unenroll"/></s:a>
		            </td>
			      	<td class="nowrap">
		            	<s:url id="mostrar" action="subject!verNotasAlumno">
			                    <s:param name="idAsignatura" value="%{id}" />
			                    <s:param name="dniAlumno" value="%{alumno.dni}" />
					    </s:url>
			            <s:a href="%{mostrar}"><s:text name="label.studentsubjects.showgrades"/></s:a>
		            </td>
		        </tr>
		    </s:iterator>
		</table>
		<s:actionerror />
		<s:actionmessage />
	</body>
</html>