<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.studentallsubjectsm.title"/> <s:property value="%{alumno.nombre}"/> (<s:property value="%{alumno.dni}"/>)</h1>
		<br/>
			<table class="borderAll">
		    <tr>
		        <th><s:text name="label.studentallsubjectsm.nid"/></th>
		        <th><s:text name="label.studentallsubjectsm.name"/></th>
		        <th><s:text name="label.studentallsubjectsm.tlf"/></th>
		    </tr>
		    <s:iterator value="alumno" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{dni}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{telefono}"/></td>
		        </tr>
		    </s:iterator>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.studentallsubjectsm.name"/></th>
		        <th><s:text name="label.studentallsubjectsm.id"/></th>
		        <th><s:text name="label.studentallsubjectsm.concept"/></th>
		        <th><s:text name="label.studentallsubjectsm.grade"/></th>
		    </tr>
		    <s:iterator value="listaNotas" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{asignatura.nombre}"/></td>
		            <td class="nowrap"><s:property value="%{id}"/></td>
		            <td class="nowrap"><s:property value="%{concepto}"/></td>
		            <td class="nowrap"><s:property value="%{nota}"/></td>
		        </tr>
		    </s:iterator>
		</table>
		<s:form action="subjectMarking!doCancel" method="POST">
		<s:submit name="idAsignatura" value="%{getText('label.studentsubjectrm.cancel')}" align="right" />
		</s:form>
		<s:actionerror />
		<s:actionmessage />
	</body>
</html>