<%@ page contentType="text/html;charset=UTF-8" language="java" import="com.opensymphony.xwork2.*, iso3.pt.service.*, iso3.pt.action.*, iso3.pt.model.*, java.util.*" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<html>
	<head>
	    <link href="<s:url value='/css/main.css'/>" rel="stylesheet" type="text/css"/>
	    <title><s:text name="application.title"/></title>
	</head>
	<body>
		<div class="titleDiv"><s:text name="application.title"/></div>
		<h1><s:text name="label.lecturersubjects.title"/> <s:property value="%{profesor.nombre}"/> (<s:property value="%{profesor.dni}"/>)</h1>
		<br/>
		<table>
			<tr>
				<td>
					<s:url id="urlLogout" action="listaAsignaturas!doLogout" escapeAmp="false"/>
					<a href="<s:property value="#urlLogout"/>"><s:text name="label.lecturersubjects.logout"/></a>
				</td>
			</tr>
		</table>
		<br/>
		<table class="borderAll">
		    <tr>
		        <th><s:text name="label.lecturersubjects.code"/></th>
		        <th><s:text name="label.lecturersubjects.name"/></th>
		        <th><s:text name="label.lecturersubjects.credits"/></th>
		        <th><s:text name="label.lecturersubjects.lecturer"/></th>
		        <th><s:text name="label.lecturersubjects.units"/></th>
		        <th><s:text name="label.lecturersubjects.students"/></th>
		        <th><s:text name="label.lecturersubjects.list"/></th>
		        <th>&nbsp;&nbsp;</th>
		    </tr>
		    <s:iterator value="listaAsignaturas" status="status">
		        <tr class="<s:if test="#status.even">even</s:if><s:else>odd</s:else>">
		            <td class="nowrap"><s:property value="%{codigo}"/></td>
		            <td class="nowrap"><s:property value="%{nombre}"/></td>
		            <td class="nowrap"><s:property value="%{creditos}"/></td>
		            <td class="nowrap"><s:property value="%{profesor.nombre}"/></td>
		            <td class="nowrap">
		            <s:url id="unidades" action="subject!generarUnidadesDeAsignatura">
			                    <s:param name="idAsignatura" value="%{id}" />
					    </s:url>
			            <s:a href="%{unidades}"><s:property value="%{listaUnidades.size}"/></s:a>
		            </td>
		            <td class="nowrap"><s:property value="%{listaAlumnos.size}"/></td>
		            <td class="nowrap">
		            	<s:url id="mostrar" action="listaAsignaturas!generarListaEstudiantes">
			                    <s:param name="idAsignatura" value="%{id}" />
					    </s:url>
			            <s:a href="%{mostrar}"><s:text name="label.lecturersubjects.list"/></s:a>
		            </td>
		        </tr>
		    </s:iterator>
		     
		</table>
		<s:actionerror />
		<s:actionmessage />
	</body>
</html>