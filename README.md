GestorUD
========

A college administration site for students and lecturers.

###Prerequisites
- Apache Tomcat
- Ant
- Hibernate
- Struts 2
- MySQL administration software (MySQL Workbench, HeidiSQL, etc)

You'll need to install at least the first two of them.

###Usage

The deployment of the project is via the Ant file. You must start your tomcat service beforehand. The `build.xml` files has the next targets:

- `prepare`  prepares the project.
- `build` is used for building.
- `jar` creates a .jar file.
- `war` creates the web application.
- `deploy` puts the war on Tomcat's webapps folder.
- `undeploy` deletes the war file.
- `clean` cleans the entire project's built files.